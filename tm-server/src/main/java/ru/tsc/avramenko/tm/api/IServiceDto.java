package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.dto.AbstractEntityDTO;

public interface IServiceDto <E extends AbstractEntityDTO> extends IRepositoryDto<E>{
}